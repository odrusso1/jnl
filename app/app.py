from flask import Flask, jsonify


app = Flask(__name__)


@app.route('/api/v1/previous', methods=["GET"])
def index():

    umm = [
        {
            "order": 1,
            "text": "i am sad",
            "colour": "#0f0f0f"
        },
        {
            "order": 2,
            "text": "i am happy",
            "colour": "#f0f0f0"
        }
    ]

    return jsonify(umm)


if __name__ == "__main__":
    app.run(debug=True)
