# JNL
Pragmatic journelling web-app

## Technology Stack
| Layer             | Technology                       |
|-------------------|----------------------------------|
| Data Layer        | Some DB, maybe probably Postgres |
| Application Layer | Python and Flask                 |
| Front End         | Create React App                 |

## Infrastructure
Google vs AWS infastructure??  
Either way I'm thinking a managed database to keep things 
easy, and a Kubernetes cluster for App hosting.